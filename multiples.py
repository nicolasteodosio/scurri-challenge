

def multiples(numbers):
    if not numbers:
        yield('Missing argument with an array of numbers')
    for number in numbers:
        try:
            if number % 3 == 0:
                if number % 5 == 0:
                    yield('ThreeFive')
                    continue
                yield('Three')
                continue
            if number % 5 == 0:
                yield('Five')
                continue
            else:
                yield(number)
        except Exception as e:
            return e


if __name__ == '__main__':
    for x in multiples(numbers=range(1, 101)):
        print(x)
    # print(multiples_return)
