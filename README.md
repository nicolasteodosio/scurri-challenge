# README #

### Tell us about one thing you are proud of in your career ###
* My answer is in answer.txt

### Running the applications ###
* This project was built using python 3.6
* Create a virtual_env using python 3
* pip install -r requirements-dev.txt

## Running multiples 3 and 5 ##
* python multiples.py

## Running api ##
* It's a WEBAPI using Vibora framework
* python api.py
* it will be running in port:8080
* For validate a post code just access localhost:8080/postcode/validate/<postcode>
* For format a post code just access localhost:8080/postcode/format/<postcode>

### Running tests ###
* python run_tests.py
