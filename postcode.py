import re

UK_POSTCODE_REGEX = re.compile(
    '^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})'
    '|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) [0-9][A-Za-z]{2})$')


class PostCode:
    def __init__(self, code):
        self.code = code

    def validate(self):
        return bool(self.find_match())

    def find_match(self):
        return UK_POSTCODE_REGEX.match(self.code)

    def format_code(self):
        self.code = self.code.upper()
        out_code = self.code[:-3].strip()
        inw_code = self.code[-3:].strip()
        self.code = out_code + ' ' + inw_code
        match = self.find_match()
        return self.code, match
