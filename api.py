from urllib.parse import unquote

from vibora import Vibora
from vibora.responses import JsonResponse

from postcode import PostCode

app = Vibora()


@app.route('/postcode/validate/<post_code>', methods=['GET'])
async def validate(post_code: str):
    post_code = unquote(post_code)
    return JsonResponse({'postcode': post_code,
                         'valid': PostCode(code=post_code).validate()})


@app.route('/postcode/format/<post_code>', methods=['GET'])
async def format_code(post_code: str):
    post_code = unquote(post_code)
    if len(post_code) < 5 or len(post_code) > 9:
        return JsonResponse({'error': 'Postcode must has between 5 and 9 digits'}, status_code=400)

    postcode, match = PostCode(code=post_code).format_code()
    if match:
            return JsonResponse({'postcode': post_code,
                                 'valid': PostCode(code=postcode).validate(),
                                 'formated_postcode': match.group(2)})
    return JsonResponse({'postcode': post_code,
                         'valid': PostCode(code=postcode).validate(),
                         'formated_postcode': postcode
                         })


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
