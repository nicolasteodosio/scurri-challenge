from unittest import TestCase
from multiples import multiples


class MultiplesTest(TestCase):

    def test_multiple_three(self):
        assert list(multiples([3])) == ['Three']

    def test_multiple_with_a_list(self):
        assert list(multiples(range(1, 16))) == [1, 2, 'Three', 4, 'Five', 'Three', 7, 8, 'Three', 'Five',
                                                 11, 'Three', 13, 14, 'ThreeFive']

    def test_multiple_with_a_list_with_only_multiples(self):
        assert list(multiples([3, 5, 15, 30, 45, 21, 9, 99, 100])) == ['Three', 'Five', 'ThreeFive', 'ThreeFive',
                                                                       'ThreeFive', 'Three', 'Three', 'Three', 'Five']

    def test_multiple_five(self):
        assert list(multiples([5])) == ['Five']

    def test_multiple_three_and_five(self):
        assert list(multiples([15])) == ['ThreeFive']

    def test_not_multiple_three_and_five(self):
        assert list(multiples([8])) == [8]

    def test_not_a_number(self):
        self.assertRaises(TypeError, multiples(['test']))

    def test_passing_an_empty_array(self):
        assert list(multiples([])) == ['Missing argument with an array of numbers']
