from vibora import TestSuite

from api import app


class ValidateTestCase(TestSuite):
    def setUp(self):
        self.client = app.test_client()

    async def test_postcode_validate_return_false(self):
        response = await self.client.get('/postcode/validate/999')
        self.assertEqual(response.content, b'{"postcode":"999","valid":false}')

    async def test_postcode_validate_return_true(self):
        response = await self.client.get('/postcode/validate/B33%208TH')
        self.assertEqual(response.content, b'{"postcode":"B33 8TH","valid":true}')

    async def test_postcode_validate_wrong_method(self):
        response = await self.client.post('/postcode/validate/B33%208TH')
        self.assertEqual(response.content, b'Method Not Allowed')


class FormatTestCase(TestSuite):
    def setUp(self):
        self.client = app.test_client()

    async def test_postcode_validate_return_length_error(self):
        response = await self.client.get('/postcode/format/999')
        self.assertEqual(response.content, b'{"error":"Postcode must has between 5 and 9 digits"}')

    async def test_postcode_validate_return_valid_and_formated_postcode(self):
        response = await self.client.get('/postcode/format/B33%208TH')
        self.assertEqual(response.content, b'{"postcode":"B33 8TH","valid":true,"formated_postcode":"B33 8TH"}')

    async def test_postcode_validate_return_valid_and_formated_postcode(self):
        response = await self.client.get('/postcode/format/9338TH')
        self.assertEqual(response.content,
                         b'{"postcode":"9338TH","valid":false,"formated_postcode":"933 8TH"}')

    async def test_postcode_validate_wrong_method(self):
        response = await self.client.post('/postcode/format/B33%208TH')
        self.assertEqual(response.content, b'Method Not Allowed')
